using Microsoft.EntityFrameworkCore;
using ThieuHoa.Models;

namespace ThieuHoa.Data
{
  public class DonHangContextMVC : DbContext
  {
    public DonHangContextMVC(DbContextOptions<DonHangContextMVC> options)
        : base(options)
    {
    }

    public DbSet<DonHang> DonHangs { get; set; }
    public DbSet<KhachHang> KhachHangs { get; set; }
    public DbSet<NhanVien> NhanViens { get; set; }
    public DbSet<Teams> Teams { get; set; }
    public DbSet<CuaHang> CuaHang { get; set; }
    public DbSet<SanPham> SanPham { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.Entity<DonHang>().ToTable("DonHangs");
      modelBuilder.Entity<KhachHang>().ToTable("KhachHangs");
      modelBuilder.Entity<NhanVien>().ToTable("NhanViens");
      modelBuilder.Entity<Teams>().ToTable("Teams");
      modelBuilder.Entity<CuaHang>().ToTable("CuaHang");
      modelBuilder.Entity<SanPham>().ToTable("SanPham");
    }
  }
}