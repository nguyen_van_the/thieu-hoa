using System;
using System.ComponentModel.DataAnnotations;

namespace ThieuHoa.Models
{
  public class SanPham
  {
    public int Id { get; set; }

    [Display(Name = "Mã SP")]
    public string MaSP { get; set; }

    public string Size { get; set; }

    [Display(Name = "Màu")]
    public string Mau { get; set; }

    [Display(Name = "Giá")]
    public decimal Price { get; set; }

    [Display(Name = "Số Lượng")]
    public int SL { get; set; }

    [Display(Name = "Mô Tả")]
    public string MoTa { get; set; }

    public string IdentityName
    {
      get
      {
        return MaSP + " - " + Size + " - " + Mau;
      }
    }
  }
}
