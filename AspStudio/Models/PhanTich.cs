using System;
using System.ComponentModel.DataAnnotations;

namespace ThieuHoa.Models
{
  public class PhanTich
  {
    public PhanTich()
    {
      ARPU = TongRE = KhachMoiRE = QuayLaiRE = L9ARPU = L1 = L7 = L9 = 0;
      L7L1 = L9L7 = TyLeSapo = TyLeSPL7 = TyLeKhachHuy = TyLeTrungDon = 0.0f;
      LenSapo = SoLuongSP = KhachHuy = TrungDon = 0;
    }

    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:N0}")]
    public decimal ARPU { get; set; }

    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:N0}")]
    [Display(Name = "RE Tổng")]
    public decimal TongRE { get; set; }

    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:N0}")]
    [Display(Name = "RE Mới")]
    public decimal KhachMoiRE { get; set; }

    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:N0}")]
    [Display(Name = "RE Quay Lại")]
    public decimal QuayLaiRE { get; set; }

    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:N0}")]
    [Display(Name = "ARPU.L9")]
    public decimal L9ARPU { get; set; }

    public decimal L1 { get; set; }
    public decimal L7 { get; set; }
    public decimal L9 { get; set; }

    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:P}")]
    [Display(Name = "L7/L1")]
    public float L7L1 { get; set; }

    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:P}")]
    [Display(Name = "L9/L7")]
    public float L9L7 { get; set; }

    [Display(Name = "Lên Sapo")]
    public int LenSapo { get; set; }

    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:P}")]
    [Display(Name = "Tỷ Lệ Sapo")]
    public float TyLeSapo { get; set; }

    [Display(Name = "Số Lượng SP")]
    public int SoLuongSP { get; set; }

    [Display(Name = "Tỷ Lệ SP/L7")]
    public float TyLeSPL7 { get; set; }

    [Display(Name = "Khách Hủy")]
    public int KhachHuy { get; set; }

    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:P}")]
    [Display(Name = "Tỷ Lệ Khách Hủy")]
    public float TyLeKhachHuy { get; set; }

    [Display(Name = "Trùng Đơn")]
    public int TrungDon { get; set; }

    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:P}")]
    [Display(Name = "Tỷ Lệ Trùng Đơn")]
    public float TyLeTrungDon { get; set; }
  }
}
