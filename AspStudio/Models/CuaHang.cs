using System;
using System.ComponentModel.DataAnnotations;

namespace ThieuHoa.Models
{
  public class CuaHang
  {
    public int Id { get; set; }

    [Display(Name = "Tên")]
    public string HoVaTen { get; set; }

    [DataType(DataType.PhoneNumber)]
    [Display(Name = "Hotline")]
    public string SDT { get; set; }

    [Display(Name = "Địa Chỉ")]
    public string DiaChi { get; set; }

    public string Ca { get; set; }

    private DateTime _createdOn = DateTime.MinValue;
    [DataType(DataType.Time)]
    [DisplayFormat(DataFormatString = "{0:hh:mm tt}", ApplyFormatInEditMode = true)]
    [Display(Name = "Giờ Mở Cửa")]
    public DateTime TimeOpen
    {
      get
      {
        return (_createdOn == DateTime.MinValue) ? DateTime.Now : _createdOn;
      }
      set { _createdOn = value; }
    }
    private DateTime _createdOff = DateTime.MinValue;
    [DataType(DataType.Time)]
    [DisplayFormat(DataFormatString = "{0:hh:mm tt}", ApplyFormatInEditMode = true)]
    [Display(Name = "Giờ Đóng Cửa")]
    public DateTime TimeClose
    {
      get
      {
        return (_createdOff == DateTime.MinValue) ? DateTime.Now : _createdOff;
      }
      set { _createdOff = value; }
    }
  }
}
