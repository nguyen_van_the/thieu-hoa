using System;
using System.ComponentModel.DataAnnotations;

namespace ThieuHoa.Models
{
  public class Teams
  {
    public int Id { get; set; }

    [Display(Name = "Tên")]
    public string HoVaTen { get; set; }


    [Display(Name = "Quản Lý")]
    public int NhanVienId { get; set; }
    [Display(Name = "Quản Lý")]
    public NhanVien NhanVienObj { get; set; }
  }
}
