using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace ThieuHoa.Models
{
  public class TongHop
  {
    public TongHop(List<DonHang> donHangs, LocThoiGian locThoiGian)
    {
      DonHangs = new List<DonHang>();
      PhanTich = new PhanTich();
      if (locThoiGian == LocThoiGian.Thang_2) //hard code => Hoi anh Hai
        PhanTich.L1 = 154;
      else if (locThoiGian == LocThoiGian.ToanBo)
        PhanTich.L1 = 154 + 2414;
      else
        PhanTich.L1 = 2414; 
      foreach (var o in donHangs)
      if (KiemTraThoiGian(o.Date, locThoiGian)) {
        DonHangs.Add(o);

        if (o.MuaLai == MuaLai.MuaMoi)
        {
          PhanTich.KhachMoiRE += o.ThanhTien;
        } else
        {
          PhanTich.QuayLaiRE += o.ThanhTien;
          PhanTich.L9 += 1;
        }
        if (!string.IsNullOrEmpty(o.HoVaTen)) {
          PhanTich.L7 += 1;
        }
        if (o.TrangThai == TrangThai.DaLenSapo)
          {
            PhanTich.LenSapo += 1;
          }
          if (o.TrangThai == TrangThai.KhachHuy)
          {
            PhanTich.KhachHuy += 1;
          }
          if (o.TrangThai == TrangThai.TrungDon)
          {
            PhanTich.TrungDon += 1;
          }
          PhanTich.SoLuongSP += 1;
      }
      if (PhanTich.L1 == 0) PhanTich.L1 = 1;
      if (PhanTich.L7 == 0) PhanTich.L7 = 1;
      if (PhanTich.SoLuongSP == 0) PhanTich.SoLuongSP = 1;
      if (PhanTich.L9 == 0) PhanTich.L9 = 1;

      PhanTich.TongRE = PhanTich.KhachMoiRE + PhanTich.QuayLaiRE;
      PhanTich.L7L1 = (float)PhanTich.L7 / (float)PhanTich.L1;
      PhanTich.L9L7 = (float)PhanTich.L9 / (float)PhanTich.L7;
      PhanTich.TyLeSPL7 = (float)PhanTich.SoLuongSP / (float)PhanTich.L7;
      PhanTich.TyLeKhachHuy = (float)PhanTich.KhachHuy / (float)PhanTich.SoLuongSP;
      PhanTich.TyLeTrungDon = (float)PhanTich.TrungDon / (float)PhanTich.SoLuongSP;
      PhanTich.TyLeSapo = (float)PhanTich.LenSapo / (float)PhanTich.SoLuongSP;
      PhanTich.ARPU = PhanTich.TongRE / PhanTich.L7;
      PhanTich.L9ARPU = PhanTich.QuayLaiRE / PhanTich.L9;
    }

    bool KiemTraThoiGian(DateTime time, LocThoiGian dieuKien)
    {
      var now = DateTime.Now;
      switch (dieuKien)
      {
        case LocThoiGian.ToanBo:
          return true;
        case LocThoiGian.HomNay:
          return time.Day == now.Day && time.Month == now.Month && time.Year == now.Year;
        case LocThoiGian.HomQua:
          time = time.AddDays(1);
          return time.Day == now.Day && time.Month == now.Month && time.Year == now.Year;
        case LocThoiGian.Thang_1:
          return time.Month == 1;
        case LocThoiGian.Thang_2:
          return time.Month == 2;
        case LocThoiGian.Thang_3:
          return time.Month == 3;
        case LocThoiGian.Thang_4:
          return time.Month == 4;
        case LocThoiGian.Thang_5:
          return time.Month == 5;
        case LocThoiGian.Thang_6:
          return time.Month == 6;
        case LocThoiGian.Thang_7:
          return time.Month == 7;
        case LocThoiGian.Thang_8:
          return time.Month == 8;
        case LocThoiGian.Thang_9:
          return time.Month == 9;
        case LocThoiGian.Thang_10:
          return time.Month == 10;
        case LocThoiGian.Thang_11:
          return time.Month == 11;
        case LocThoiGian.Thang_12:
          return time.Month == 12;
      }
      return false;
    }
    public List<DonHang> DonHangs { get; set; }
    public PhanTich PhanTich { get; set; }
  }
}
