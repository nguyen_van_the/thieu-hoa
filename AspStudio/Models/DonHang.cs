using System;
using System.ComponentModel.DataAnnotations;

namespace ThieuHoa.Models
{
  public class DonHang
  {
    public int Id { get; set; }

    private DateTime _createdOn = DateTime.MinValue;
    [DataType(DataType.Date)]
    [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    [Display(Name = "Date")]
    public DateTime Date
    {
      get
      {
        return (_createdOn == DateTime.MinValue) ? DateTime.Now : _createdOn;
      }
      set { _createdOn = value; }
    }

    [Display(Name = "Mua Lại")]
    public MuaLai MuaLai { get; set; }

    [Display(Name = "Cửa Hàng Bán")]
    public int CuaHangId { get; set; }
    [Display(Name = "Nhân Viên")]
    public int NhanVienId { get; set; }
    [Display(Name = "Sản Phẩm")]
    public int SanPhamId { get; set; }
    [Display(Name = "Khách Hàng")]
    public int KhachHangId { get; set; }

    [Display(Name = "Số Lượng")]
    public int SL { get; set; }

    [Display(Name = "Trạng Thái")]
    public TrangThai TrangThai { get; set; }

    [Display(Name = "Lưu Ý")]
    public string LuuYGiaoHang { get; set; }

    [Display(Name = "Giá Sản Phẩm")]
    public decimal GiaSanPham { get; set; }

    [Display(Name = "Nguồn Hàng")]
    public string Nguon { get; set; }
    //public NguonHang Nguon { get; set; }

    [Display(Name = "Họ và Tên")]
    public string HoVaTen { get; set; }

    [DataType(DataType.PhoneNumber)]
    [Display(Name = "Số Điện Thoại")]
    public string SDT { get; set; }

    [Display(Name = "Địa Chỉ")]
    public string DiaChi { get; set; }

    public string Size { get; set; }
    //public Size Size { get; set; }

    [Display(Name = "Màu")]
    public string Mau { get; set; }

    [Display(Name = "Mã Sản Phẩm")]
    public string MaSP { get; set; }

    [Display(Name = "Thành Tiền")]
    public decimal ThanhTien { get; set; }
  }

  //public class NguonHang
  //{
  //  public int Id { get; set; }
  //  public string Nguon { get; set; }
  //}

  //public class Team
  //{
  //  public int Id { get; set; }
  //  public string Ten { get; set; }
  //}

  public enum LocThoiGian
  {
    [Display(Name = "Toàn Bộ")]
    ToanBo = 0,
    [Display(Name = "Tháng 1")]
    Thang_1,
    [Display(Name = "Tháng 2")]
    Thang_2,
    [Display(Name = "Tháng 3")]
    Thang_3,
    [Display(Name = "Tháng 4")]
    Thang_4,
    [Display(Name = "Tháng 5")]
    Thang_5,
    [Display(Name = "Tháng 6")]
    Thang_6,
    [Display(Name = "Tháng 7")]
    Thang_7,
    [Display(Name = "Tháng 8")]
    Thang_8,
    [Display(Name = "Tháng 9")]
    Thang_9,
    [Display(Name = "Tháng 10")]
    Thang_10,
    [Display(Name = "Tháng 11")]
    Thang_11,
    [Display(Name = "Tháng 12")]
    Thang_12,
    [Display(Name = "Hôm Nay")]
    HomNay = 13,
    [Display(Name = "Hôm Qua")]
    HomQua = 14,
  }

  public enum MuaLai
  {
    [Display(Name = "Mua Mới")]
    MuaMoi = 0,
    [Display(Name = "Mua Lại")]
    MuaLai = 1
  }

  public enum Team
  {
    [Display(Name = "Như")]
    Nhu,
    [Display(Name = "Xuân")]
    Xuan,
    [Display(Name = "Tú")]
    Tu,
    [Display(Name = "Trinh")]
    Trinh,
    [Display(Name = "Hân")]
    Han,
    [Display(Name = "HN Ly")]
    HN_Ly,
    [Display(Name = "HN Hải")]
    HN_Hai,
    [Display(Name = "Hưng")]
    Hung,
    [Display(Name = "Nghỉ Việc")]
    NghiViec,
    [Display(Name = "Khác")]
    Others
  }

  public enum NguonHang
  {
    [Display(Name = "FB Thiều Hoa")]
    ThieuHoaFB = 0,
    [Display(Name = "Cửa Hàng SG")]
    CuaHangSG = 1,
    [Display(Name = "Cửa Hàng HN")]
    CuaHangHN = 2,
    [Display(Name = "Web")]
    Web = 3,
    [Display(Name = "LDP Thiều Hoa")]
    ThieuHoaLDP = 4,
    Zalo,
    Hotline,
    LuaThuongUyenFB,
    ThoiTrangCaoCapThieuHoaFB,
    XuHuongSacDepFB,
    SacDepPhuNuFB,
    KhanChoangSgFB,
    ThieuHoaDangCapQuyPhaiFB,
    DressByAnne,
    Chloestore,
    ThoiThuongFB,
    PhongCachThoiTrang
  }

  public enum Size
  {
    M = 0,
    L = 1,
    XL = 2,
    XXL = 3,
    XXXL = 4
  }

  public enum TrangThai
  {
    [Display(Name = "Đã Lên Sapo")]
    DaLenSapo = 0,
    [Display(Name = "Khách Hủy")]
    KhachHuy = 1,
    [Display(Name = "Trùng Đơn")]
    TrungDon = 2,
    [Display(Name = "Test")]
    Test = 3
  }
}