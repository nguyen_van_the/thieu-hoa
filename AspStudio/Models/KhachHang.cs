using System;
using System.ComponentModel.DataAnnotations;

namespace ThieuHoa.Models
{
  public class KhachHang
  {
    public int Id { get; set; }

    [Display(Name = "Họ và Tên")]
    public string HoVaTen { get; set; }

    [DataType(DataType.PhoneNumber)]
    [Display(Name = "Số Điện Thoại")]
    public string SDT { get; set; }

    [Display(Name = "Địa Chỉ")]
    public string DiaChi { get; set; }

    public string IdentityName
    {
      get
      {
        return HoVaTen + " - " + SDT;
      }
    }
  }
}
