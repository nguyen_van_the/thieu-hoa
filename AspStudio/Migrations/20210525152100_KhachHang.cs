﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ThieuHoa.Migrations
{
    public partial class KhachHang : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
          migrationBuilder.CreateTable(
          name: "KhachHangs",
          columns: table => new
          {
            Id = table.Column<int>(nullable: false)
                  .Annotation("Sqlite:Autoincrement", true),
            HoVaTen = table.Column<string>(nullable: true),
            SDT = table.Column<string>(nullable: true),
            DiaChi = table.Column<string>(nullable: true),
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_KhachHangs", x => x.Id);
          });
        //migrationBuilder.AlterColumn<string>(
      //    name: "SDT",
      //    table: "KhachHangs",
      //    nullable: true,
      //    oldClrType: typeof(string),
      //    oldType: "nvarchar(max)",
      //    oldNullable: true);

      //migrationBuilder.AlterColumn<string>(
      //    name: "HoVaTen",
      //    table: "KhachHangs",
      //    nullable: true,
      //    oldClrType: typeof(string),
      //    oldType: "nvarchar(max)",
      //    oldNullable: true);

      //migrationBuilder.AlterColumn<string>(
      //    name: "DiaChi",
      //    table: "KhachHangs",
      //    nullable: true,
      //    oldClrType: typeof(string),
      //    oldType: "nvarchar(max)",
      //    oldNullable: true);

      //migrationBuilder.AlterColumn<int>(
      //    name: "Id",
      //    table: "KhachHangs",
      //    nullable: false,
      //    oldClrType: typeof(int),
      //    oldType: "int")
      //    .Annotation("Sqlite:Autoincrement", true)
      //    .OldAnnotation("Sqlite:Autoincrement", true);

      //migrationBuilder.AlterColumn<int>(
      //    name: "TrangThai",
      //    table: "DonHangs",
      //    nullable: false,
      //    oldClrType: typeof(int),
      //    oldType: "int");

      //migrationBuilder.AlterColumn<decimal>(
      //    name: "ThanhTien",
      //    table: "DonHangs",
      //    nullable: false,
      //    oldClrType: typeof(decimal),
      //    oldType: "decimal(18,2)");

      //migrationBuilder.AlterColumn<int>(
      //    name: "Size",
      //    table: "DonHangs",
      //    nullable: false,
      //    oldClrType: typeof(int),
      //    oldType: "int");

      //migrationBuilder.AlterColumn<int>(
      //    name: "SL",
      //    table: "DonHangs",
      //    nullable: false,
      //    oldClrType: typeof(int),
      //    oldType: "int");

      //migrationBuilder.AlterColumn<string>(
      //    name: "SDT",
      //    table: "DonHangs",
      //    nullable: true,
      //    oldClrType: typeof(string),
      //    oldType: "nvarchar(max)",
      //    oldNullable: true);

      //migrationBuilder.AlterColumn<int>(
      //    name: "Nguon",
      //    table: "DonHangs",
      //    nullable: false,
      //    oldClrType: typeof(int),
      //    oldType: "int");

      //migrationBuilder.AlterColumn<int>(
      //    name: "MuaLai",
      //    table: "DonHangs",
      //    nullable: false,
      //    oldClrType: typeof(int),
      //    oldType: "int");

      //migrationBuilder.AlterColumn<string>(
      //    name: "Mau",
      //    table: "DonHangs",
      //    nullable: true,
      //    oldClrType: typeof(string),
      //    oldType: "nvarchar(max)",
      //    oldNullable: true);

      //migrationBuilder.AlterColumn<string>(
      //    name: "MaSP",
      //    table: "DonHangs",
      //    nullable: true,
      //    oldClrType: typeof(string),
      //    oldType: "nvarchar(max)",
      //    oldNullable: true);

      //migrationBuilder.AlterColumn<string>(
      //    name: "LuuYGiaoHang",
      //    table: "DonHangs",
      //    nullable: true,
      //    oldClrType: typeof(string),
      //    oldType: "nvarchar(max)",
      //    oldNullable: true);

      //migrationBuilder.AlterColumn<string>(
      //    name: "HoVaTen",
      //    table: "DonHangs",
      //    nullable: true,
      //    oldClrType: typeof(string),
      //    oldType: "nvarchar(max)",
      //    oldNullable: true);

      //migrationBuilder.AlterColumn<decimal>(
      //    name: "GiaSanPham",
      //    table: "DonHangs",
      //    nullable: false,
      //    oldClrType: typeof(decimal),
      //    oldType: "decimal(18,2)");

      //migrationBuilder.AlterColumn<string>(
      //    name: "DiaChi",
      //    table: "DonHangs",
      //    nullable: true,
      //    oldClrType: typeof(string),
      //    oldType: "nvarchar(max)",
      //    oldNullable: true);

      //migrationBuilder.AlterColumn<DateTime>(
      //    name: "Date",
      //    table: "DonHangs",
      //    nullable: false,
      //    oldClrType: typeof(DateTime),
      //    oldType: "datetime2");

      //migrationBuilder.AlterColumn<int>(
      //    name: "Id",
      //    table: "DonHangs",
      //    nullable: false,
      //    oldClrType: typeof(int),
      //    oldType: "int")
      //    .Annotation("Sqlite:Autoincrement", true)
      //    .OldAnnotation("Sqlite:Autoincrement", true);
    }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
      migrationBuilder.DropTable(
      name: "KhachHangs");
      //migrationBuilder.AlterColumn<string>(
      //    name: "SDT",
      //    table: "KhachHangs",
      //    type: "nvarchar(max)",
      //    nullable: true,
      //    oldClrType: typeof(string),
      //    oldNullable: true);

      //migrationBuilder.AlterColumn<string>(
      //    name: "HoVaTen",
      //    table: "KhachHangs",
      //    type: "nvarchar(max)",
      //    nullable: true,
      //    oldClrType: typeof(string),
      //    oldNullable: true);

      //migrationBuilder.AlterColumn<string>(
      //    name: "DiaChi",
      //    table: "KhachHangs",
      //    type: "nvarchar(max)",
      //    nullable: true,
      //    oldClrType: typeof(string),
      //    oldNullable: true);

      //migrationBuilder.AlterColumn<int>(
      //    name: "Id",
      //    table: "KhachHangs",
      //    type: "int",
      //    nullable: false,
      //    oldClrType: typeof(int))
      //    .Annotation("Sqlite:Autoincrement", true)
      //    .OldAnnotation("Sqlite:Autoincrement", true);

      //migrationBuilder.AlterColumn<int>(
      //    name: "TrangThai",
      //    table: "DonHangs",
      //    type: "int",
      //    nullable: false,
      //    oldClrType: typeof(int));

      //migrationBuilder.AlterColumn<decimal>(
      //    name: "ThanhTien",
      //    table: "DonHangs",
      //    type: "decimal(18,2)",
      //    nullable: false,
      //    oldClrType: typeof(decimal));

      //migrationBuilder.AlterColumn<int>(
      //    name: "Size",
      //    table: "DonHangs",
      //    type: "int",
      //    nullable: false,
      //    oldClrType: typeof(int));

      //migrationBuilder.AlterColumn<int>(
      //    name: "SL",
      //    table: "DonHangs",
      //    type: "int",
      //    nullable: false,
      //    oldClrType: typeof(int));

      //migrationBuilder.AlterColumn<string>(
      //    name: "SDT",
      //    table: "DonHangs",
      //    type: "nvarchar(max)",
      //    nullable: true,
      //    oldClrType: typeof(string),
      //    oldNullable: true);

      //migrationBuilder.AlterColumn<int>(
      //    name: "Nguon",
      //    table: "DonHangs",
      //    type: "int",
      //    nullable: false,
      //    oldClrType: typeof(int));

      //migrationBuilder.AlterColumn<int>(
      //    name: "MuaLai",
      //    table: "DonHangs",
      //    type: "int",
      //    nullable: false,
      //    oldClrType: typeof(int));

      //migrationBuilder.AlterColumn<string>(
      //    name: "Mau",
      //    table: "DonHangs",
      //    type: "nvarchar(max)",
      //    nullable: true,
      //    oldClrType: typeof(string),
      //    oldNullable: true);

      //migrationBuilder.AlterColumn<string>(
      //    name: "MaSP",
      //    table: "DonHangs",
      //    type: "nvarchar(max)",
      //    nullable: true,
      //    oldClrType: typeof(string),
      //    oldNullable: true);

      //migrationBuilder.AlterColumn<string>(
      //    name: "LuuYGiaoHang",
      //    table: "DonHangs",
      //    type: "nvarchar(max)",
      //    nullable: true,
      //    oldClrType: typeof(string),
      //    oldNullable: true);

      //migrationBuilder.AlterColumn<string>(
      //    name: "HoVaTen",
      //    table: "DonHangs",
      //    type: "nvarchar(max)",
      //    nullable: true,
      //    oldClrType: typeof(string),
      //    oldNullable: true);

      //migrationBuilder.AlterColumn<decimal>(
      //    name: "GiaSanPham",
      //    table: "DonHangs",
      //    type: "decimal(18,2)",
      //    nullable: false,
      //    oldClrType: typeof(decimal));

      //migrationBuilder.AlterColumn<string>(
      //    name: "DiaChi",
      //    table: "DonHangs",
      //    type: "nvarchar(max)",
      //    nullable: true,
      //    oldClrType: typeof(string),
      //    oldNullable: true);

      //migrationBuilder.AlterColumn<DateTime>(
      //    name: "Date",
      //    table: "DonHangs",
      //    type: "datetime2",
      //    nullable: false,
      //    oldClrType: typeof(DateTime));

      //migrationBuilder.AlterColumn<int>(
      //    name: "Id",
      //    table: "DonHangs",
      //    type: "int",
      //    nullable: false,
      //    oldClrType: typeof(int))
      //    .Annotation("Sqlite:Autoincrement", true)
      //    .OldAnnotation("Sqlite:Autoincrement", true);
    }
    }
}
