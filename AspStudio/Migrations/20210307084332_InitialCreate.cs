﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ThieuHoa.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DonHangs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Date = table.Column<DateTime>(nullable: false),
                    MuaLai = table.Column<int>(nullable: false),
                    GiaSanPham = table.Column<decimal>(nullable: false),
                    Nguon = table.Column<int>(nullable: false),
                    HoVaTen = table.Column<string>(nullable: true),
                    SDT = table.Column<string>(nullable: true),
                    DiaChi = table.Column<string>(nullable: true),
                    Size = table.Column<int>(nullable: false),
                    Mau = table.Column<string>(nullable: true),
                    SL = table.Column<int>(nullable: false),
                    MaSP = table.Column<string>(nullable: true),
                    ThanhTien = table.Column<decimal>(nullable: false),
                    TrangThai = table.Column<int>(nullable: false),
                    LuuYGiaoHang = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DonHangs", x => x.Id);
                });

          migrationBuilder.CreateTable(
          name: "KhachHangs",
          columns: table => new
          {
            Id = table.Column<int>(nullable: false)
                  .Annotation("Sqlite:Autoincrement", true),
            HoVaTen = table.Column<string>(nullable: true),
            SDT = table.Column<string>(nullable: true),
            DiaChi = table.Column<string>(nullable: true),
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_KhachHangs", x => x.Id);
          });
    }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DonHangs");
            migrationBuilder.DropTable(
                  name: "KhachHangs");
          }
    }
}
