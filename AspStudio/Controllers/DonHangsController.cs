using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ThieuHoa.Data;
using ThieuHoa.Models;

namespace ThieuHoa.Controllers
{
    public class DonHangsController : Controller
    {
        private readonly DonHangContextMVC _context;

        public DonHangsController(DonHangContextMVC context)
        {
            _context = context;
        }

        // GET: DonHangs
        public async Task<IActionResult> Index(int TimeRange=0)
        {
            //Setup Parameter for Viewer
            var listTimeFilter = new List<SelectListItem>
                      {
                          new SelectListItem {Text = "Toàn Bộ", Value = "0"},
                          new SelectListItem {Text = "Hôm Nay", Value = "13"},
                          new SelectListItem {Text = "Hôm Qua", Value = "14"},
                          new SelectListItem {Text = "Tháng 1", Value = "1"},
                          new SelectListItem {Text = "Tháng 2", Value = "2"},
                          new SelectListItem {Text = "Tháng 3", Value = "3"},
                          new SelectListItem {Text = "Tháng 4", Value = "4"},
                          new SelectListItem {Text = "Tháng 5", Value = "5"},
                          new SelectListItem {Text = "Tháng 6", Value = "6"},
                          new SelectListItem {Text = "Tháng 7", Value = "7"},
                          new SelectListItem {Text = "Tháng 8", Value = "8"},
                          new SelectListItem {Text = "Tháng 9", Value = "9"},
                          new SelectListItem {Text = "Tháng 10", Value = "10"},
                          new SelectListItem {Text = "Tháng 11", Value = "11"},
                          new SelectListItem {Text = "Tháng 12", Value = "12"}
                      };
            ViewBag.TimeRangeList = listTimeFilter;
            //Main
            var donHangs = await _context.DonHangs.ToArrayAsync();
            return View(new TongHop(donHangs.ToList(), (LocThoiGian)TimeRange));
        }

        // GET: DonHangs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var donHang = await _context.DonHangs
                .FirstOrDefaultAsync(m => m.Id == id);
            if (donHang == null)
            {
                return NotFound();
            }

            return View(donHang);
        }

        // GET: DonHangs/Create
        public IActionResult Create()
        {

          // Set vào ViewBag
          ViewBag.MuaLaiList = listMuaLai;
          ViewBag.NguonHangList = listNguonHang;
          ViewBag.SizeList = listSize;
          ViewBag.TrangThaiList = listTrangThai;
          ViewData["CuaHangId"] = new SelectList(_context.CuaHang, "Id", "HoVaTen");
          ViewData["NhanVienId"] = new SelectList(_context.NhanViens, "Id", "IdentityName");
          ViewData["SanPhamId"] = new SelectList(_context.SanPham, "Id", "IdentityName");
          ViewData["KhachHangId"] = new SelectList(_context.KhachHangs, "Id", "IdentityName");          
          return View();
        }

        // POST: DonHangs/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
    //public async Task<IActionResult> Create([Bind("Id,Date,MuaLai,Nguon,HoVaTen,DiaChi,Size,Mau,MaSP,GiaSanPham,ThanhTien,LenDonSapo,LuuYGiaoHang,Ngay,Thang,STT,SDT,SL")] DonHang donHang),GiaSanPham,Nguon,HoVaTen,SDT,DiaChi,Size,Mau,MaSP,ThanhTien
    public async Task<IActionResult> Create([Bind("Id,Date,MuaLai,CuaHangId,NhanVienId,SanPhamId,KhachHangId,SL,TrangThai,LuuYGiaoHang")] DonHang donHang)
    {
      if (ModelState.IsValid)
            {
                var cuaHang = await _context.CuaHang.FirstOrDefaultAsync(m => m.Id == donHang.CuaHangId);
                var nhanVien = await _context.NhanViens.FirstOrDefaultAsync(m => m.Id == donHang.NhanVienId);
                var sanPham = await _context.SanPham.FirstOrDefaultAsync(m => m.Id == donHang.SanPhamId);
                var khachHang = await _context.KhachHangs.FirstOrDefaultAsync(m => m.Id == donHang.KhachHangId);
                donHang.DiaChi = khachHang.DiaChi;
                donHang.GiaSanPham = sanPham.Price;
                donHang.HoVaTen = khachHang.HoVaTen;
                donHang.MaSP = sanPham.MaSP;
                donHang.Mau = sanPham.Mau;
                donHang.Nguon = cuaHang.HoVaTen;
                donHang.SDT = khachHang.SDT;
                donHang.Size = sanPham.Size;
                donHang.ThanhTien = sanPham.Price * donHang.SL;
                _context.Add(donHang);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(donHang);
        }

        // GET: DonHangs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            // Set vào ViewBag
            ViewBag.MuaLaiList = listMuaLai;
            ViewBag.NguonHangList = listNguonHang;
            ViewBag.SizeList = listSize;
            ViewBag.TrangThaiList = listTrangThai;
            if (id == null)
            {
                return NotFound();
            }

            var donHang = await _context.DonHangs.FindAsync(id);
            if (donHang == null)
            {
                return NotFound();
            }
            return View(donHang);
        }

        // POST: DonHangs/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Date,MuaLai,Nguon,HoVaTen,DiaChi,Size,Mau,MaSP,GiaSanPham,ThanhTien,LenDonSapo,LuuYGiaoHang,Ngay,Thang,STT,SDT,SL")] DonHang donHang)
        {
            if (id != donHang.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(donHang);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DonHangExists(donHang.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(donHang);
        }

        // GET: DonHangs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var donHang = await _context.DonHangs
                .FirstOrDefaultAsync(m => m.Id == id);
            if (donHang == null)
            {
                return NotFound();
            }

            return View(donHang);
        }

        // POST: DonHangs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var donHang = await _context.DonHangs.FindAsync(id);
            _context.DonHangs.Remove(donHang);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DonHangExists(int id)
        {
            return _context.DonHangs.Any(e => e.Id == id);
        }

    List<SelectListItem> listMuaLai = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Mua Mới", Value = "0"},
                    new SelectListItem {Text = "Mua Lại", Value = "1"}
                };
    List<SelectListItem> listNguonHang = new List<SelectListItem>
                {
                    new SelectListItem {Text = "FB Thiều Hoa", Value = "0"},
                    new SelectListItem {Text = "Cửa Hàng SG", Value = "1"},
                    new SelectListItem {Text = "Cửa Hàng HN", Value = "2"},
                    new SelectListItem {Text = "Web", Value = "3"},
                    new SelectListItem {Text = "LDP Thiều Hoa", Value = "4"}
                };
    List<SelectListItem> listSize = new List<SelectListItem>
                {
                    new SelectListItem {Text = "M", Value = "0"},
                    new SelectListItem {Text = "L", Value = "1"},
                    new SelectListItem {Text = "XL", Value = "2"},
                    new SelectListItem {Text = "XXL", Value = "3"},
                    new SelectListItem {Text = "XXXL", Value = "4"}
                };
    List<SelectListItem> listTrangThai = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Đã Lên Sapo", Value = "0"},
                    new SelectListItem {Text = "Khách Hủy", Value = "1"},
                    new SelectListItem {Text = "Trùng Đơn", Value = "2"},
                    new SelectListItem {Text = "Test", Value = "3"}
                };
    List<SelectListItem> listMau = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Cam", Value = "Cam"},
                    new SelectListItem {Text = "Đen", Value = "Đen"},
                    new SelectListItem {Text = "Đỏ", Value = "Đỏ"},
                    new SelectListItem {Text = "Hồng", Value = "Hồng"},
                    new SelectListItem {Text = "Vàng", Value = "Vàng"},
                    new SelectListItem {Text = "Nâu", Value = "Nâu"},
                    new SelectListItem {Text = "Xám", Value = "Xám"},
                    new SelectListItem {Text = "Rêu", Value = "Rêu"},
                    new SelectListItem {Text = "Tím", Value = "Tím"},
                    new SelectListItem {Text = "Kem", Value = "Kem"},
                    new SelectListItem {Text = "Trắng", Value = "Trắng"},
                    new SelectListItem {Text = "Trà", Value = "Trà"},
                    new SelectListItem {Text = "Xanh Dương", Value = "Xanh Dương"},
                    new SelectListItem {Text = "Xanh Lá", Value = "Xanh Lá"},
                    new SelectListItem {Text = "Xanh Đen", Value = "Xanh Đen"},
                    new SelectListItem {Text = "Xanh Đậm", Value = "Xanh Đậm"},
                    new SelectListItem {Text = "Xanh", Value = "Xanh"},
                    new SelectListItem {Text = "Tím Đậm", Value = "Tím Đậm"},
                    new SelectListItem {Text = "Xanh Than", Value = "Xanh Than"},
                    new SelectListItem {Text = "Hồng ĐấT", Value = "Hồng ĐấT"},
                    new SelectListItem {Text = "Đỏ Nâu", Value = "Đỏ Nâu"}
                };
  }
}
