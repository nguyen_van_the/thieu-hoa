using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ThieuHoa.Data;
using ThieuHoa.Models;

namespace ThieuHoa.Controllers
{
    public class SanPhamController : Controller
    {
        private readonly DonHangContextMVC _context;

        public SanPhamController(DonHangContextMVC context)
        {
            _context = context;
        }

        // GET: SanPham
        public async Task<IActionResult> Index()
        {
            return View(await _context.SanPham.ToListAsync());
        }

        // GET: SanPham/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sanPham = await _context.SanPham
                .FirstOrDefaultAsync(m => m.Id == id);
            if (sanPham == null)
            {
                return NotFound();
            }

            return View(sanPham);
        }

        // GET: SanPham/Create
        public IActionResult Create()
        {
            ViewBag.SizeList = listSize;
            ViewBag.ListMau = listMau;
            return View();
        }

        // POST: SanPham/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,MaSP,Size,Mau,Price,SL,MoTa")] SanPham sanPham)
        {
            if (ModelState.IsValid)
            {
                _context.Add(sanPham);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(sanPham);
        }

        // GET: SanPham/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sanPham = await _context.SanPham.FindAsync(id);
            if (sanPham == null)
            {
                return NotFound();
            }
            ViewBag.SizeList = listSize;
            ViewBag.ListMau = listMau;
            return View(sanPham);
        }

        // POST: SanPham/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,MaSP,Size,Mau,Price,SL,MoTa")] SanPham sanPham)
        {
            if (id != sanPham.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(sanPham);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SanPhamExists(sanPham.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(sanPham);
        }

        // GET: SanPham/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sanPham = await _context.SanPham
                .FirstOrDefaultAsync(m => m.Id == id);
            if (sanPham == null)
            {
                return NotFound();
            }

            return View(sanPham);
        }

        // POST: SanPham/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var sanPham = await _context.SanPham.FindAsync(id);
            _context.SanPham.Remove(sanPham);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SanPhamExists(int id)
        {
            return _context.SanPham.Any(e => e.Id == id);
        }

        List<SelectListItem> listSize = new List<SelectListItem>
              {
                  new SelectListItem {Text = "M", Value = "M"},
                  new SelectListItem {Text = "L", Value = "L"},
                  new SelectListItem {Text = "XL", Value = "XL"},
                  new SelectListItem {Text = "XXL", Value = "XXL"},
                  new SelectListItem {Text = "XXXL", Value = "XXXL"}
              };
        List<SelectListItem> listMau = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Cam", Value = "Cam"},
                    new SelectListItem {Text = "Đen", Value = "Đen"},
                    new SelectListItem {Text = "Đỏ", Value = "Đỏ"},
                    new SelectListItem {Text = "Hồng", Value = "Hồng"},
                    new SelectListItem {Text = "Vàng", Value = "Vàng"},
                    new SelectListItem {Text = "Nâu", Value = "Nâu"},
                    new SelectListItem {Text = "Xám", Value = "Xám"},
                    new SelectListItem {Text = "Rêu", Value = "Rêu"},
                    new SelectListItem {Text = "Tím", Value = "Tím"},
                    new SelectListItem {Text = "Kem", Value = "Kem"},
                    new SelectListItem {Text = "Trắng", Value = "Trắng"},
                    new SelectListItem {Text = "Trà", Value = "Trà"},
                    new SelectListItem {Text = "Xanh Dương", Value = "Xanh Dương"},
                    new SelectListItem {Text = "Xanh Lá", Value = "Xanh Lá"},
                    new SelectListItem {Text = "Xanh Đen", Value = "Xanh Đen"},
                    new SelectListItem {Text = "Xanh Đậm", Value = "Xanh Đậm"},
                    new SelectListItem {Text = "Xanh", Value = "Xanh"},
                    new SelectListItem {Text = "Tím Đậm", Value = "Tím Đậm"},
                    new SelectListItem {Text = "Xanh Than", Value = "Xanh Than"},
                    new SelectListItem {Text = "Hồng ĐấT", Value = "Hồng ĐấT"},
                    new SelectListItem {Text = "Đỏ Nâu", Value = "Đỏ Nâu"}
                };
  }
}
