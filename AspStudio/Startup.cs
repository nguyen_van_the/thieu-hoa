using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using ThieuHoa.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace ThieuHoa
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Environment = env;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
        		services.AddMvc(options => options.EnableEndpointRouting = false);  
            services.AddControllersWithViews();
            services.AddRazorPages();

            services.AddDbContext<DonHangContextMVC>(options =>
            {
              var connectionString = Configuration.GetConnectionString("DonHangContextMVC");
              var connectionStringServer = Configuration.GetConnectionString("DonHangContextMVC_Server");

              if (Environment.IsDevelopment())
              {
                options.UseSqlite(connectionString);
              }
              else
              {
                options.UseSqlServer(connectionStringServer);
              }
            });
          }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
        app.UseDatabaseErrorPage();
      }
      else
      {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();
            
            app.UseEndpoints(endpoints =>
            {
            		endpoints.MapAreaControllerRoute(  
										name: "Identity",  
										areaName: "Identity",  
										pattern: "Identity/{controller=Home}/{action=Index}");
                endpoints.MapAreaControllerRoute(
                    name: "DonHang",
                    areaName: "BanHang",
                    pattern: "BanHang/{controller=Home}/{action=Index}");
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}");
                endpoints.MapRazorPages();
            });
        }
    }
}
