﻿#pragma checksum "/Volumes/DataMac/Git/thieu-hoa/asp_studio_v2.0/template_asp/AspStudio/Views/Pages/SearchResults.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "cfcfb0819c0593e75a98a2bb27591946791d7b1f"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Pages_SearchResults), @"mvc.1.0.view", @"/Views/Pages/SearchResults.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "/Volumes/DataMac/Git/thieu-hoa/asp_studio_v2.0/template_asp/AspStudio/Views/_ViewImports.cshtml"
using ThieuHoa;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "/Volumes/DataMac/Git/thieu-hoa/asp_studio_v2.0/template_asp/AspStudio/Views/_ViewImports.cshtml"
using ThieuHoa.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"cfcfb0819c0593e75a98a2bb27591946791d7b1f", @"/Views/Pages/SearchResults.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1b4bb36166bf7968ff5fe2d0d77ee44f6368831a", @"/Views/_ViewImports.cshtml")]
    public class Views_Pages_SearchResults : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("action", new global::Microsoft.AspNetCore.Html.HtmlString("#"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "POST", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("name", new global::Microsoft.AspNetCore.Html.HtmlString("search_form"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "/Volumes/DataMac/Git/thieu-hoa/asp_studio_v2.0/template_asp/AspStudio/Views/Pages/SearchResults.cshtml"
  
    ViewData["Title"] = "Search Results";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<ul class=""breadcrumb"">
	<li class=""breadcrumb-item""><a href=""#"">PAGES</a></li>
	<li class=""breadcrumb-item active"">SEARCH RESULTS</li>
</ul>

<h1 class=""page-header"">
	Search Results <small>page header description goes here...</small>
</h1>

<!-- BEGIN search-result -->
<div class=""search-result"">
	<!-- BEGIN search-input -->
	<div class=""search-input"">
		");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "cfcfb0819c0593e75a98a2bb27591946791d7b1f4837", async() => {
                WriteLiteral("\r\n\t\t\t<a href=\"#\" class=\"search-close\" data-clear-form=\"#search\">&times;</a>\r\n\t\t\t<input type=\"text\" class=\"form-control form-control-lg\" value=\"MacBook\" />\r\n\t\t");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
	</div>
	<!-- END search-input -->
	
	<!-- BEGIN search-tab -->
	<div class=""search-tab"">
		<div class=""search-tab-item""><a href=""#"" class=""search-tab-link active"">Explore</a></div>
		<div class=""search-tab-item""><a href=""#"" class=""search-tab-link"">Accessories</a></div>
		<div class=""search-tab-item""><a href=""#"" class=""search-tab-link"">Support</a></div>
		<div class=""search-tab-item""><a href=""#"" class=""search-tab-link"">Store</a></div>
	</div>
	<!-- END search-tab -->
	
	<!-- BEGIN search-result -->
	<div class=""search-result"">
		<!-- BEGIN search-result-list -->
		<div class=""search-result-list"">
			<div class=""search-result-item"">
				<div class=""search-result-media"">
					<a href=""#""><img src=""/img/product/product.png""");
            BeginWriteAttribute("alt", " alt=\"", 1389, "\"", 1395, 0);
            EndWriteAttribute();
            WriteLiteral(@" /></a>
				</div>
				<div class=""search-result-content"">
					<h3><a href=""#"">MacBook</a></h3>
					<p>
						MacBook features sixth-generation Intel Core processors, improved graphics performance, and up to 10 hours of battery life.
					</p>
					<a href=""#"" class=""me-4"">Learn more</a>
					<a href=""#"" class=""me-4"">MacBook Buy</a>
					<a href=""#"">MacBook Support</a>
				</div>
			</div>
			<div class=""search-result-item"">
				<div class=""search-result-content"">
					<h3><a href=""#"">MacBook - Apple</a></h3>
					<p>
						The incredibly thin and light MacBook features sixth-generation processors, improved graphics performance, and up to 10 hours of battery life....
					</p>
					<a href=""#"">https://www.apple.com/macbook/</a>
				</div>
			</div>
			<div class=""search-result-item"">
				<div class=""search-result-content"">
					<h3><a href=""#"">MacBook Pro - macOS - Apple</a></h3>
					<p>
						MacBook Pro - macOS - Apple macOS. It’s why there’s nothing else like a Mac. macOS is the");
            WriteLiteral(@" operating system that powers every Mac. It lets you do things you simply can’t with other computers. That’s...
					</p>
					<a href=""#"">https://www.apple.com/macbook-pro/macos/</a>
				</div>
			</div>
			<div class=""search-result-item"">
				<div class=""search-result-content"">
					<h3><a href=""#"">MacBook Air - Design - Apple</a></h3>
					<p>
						MacBook Air is incredibly thin and light. But it’s also powerful, durable, and enjoyable to use. With enough battery life to get you through the day....
					</p>
					<a href=""#"">https://www.apple.com/macbook-air/design/</a>
				</div>
			</div>
			<div class=""search-result-item"">
				<div class=""search-result-content"">
					<h3><a href=""#"">MacBook Air - Performance - Apple</a></h3>
					<p>
						MacBook Air is packed with powerful components that give you smoother graphics, faster storage, and all-around serious performance....
					</p>
					<a href=""#"">https://www.apple.com/macbook-air/performance/</a>
				</div>
			</div>
			<div cl");
            WriteLiteral(@"ass=""search-result-item"">
				<div class=""search-result-content"">
					<h3><a href=""#"">MacBook Air - Technical Specifications - Apple</a></h3>
					<p>
						View the technical specifications for MacBook Air, see what comes in the box, explore configuration options, and get a list of accessories....
					</p>
					<a href=""#"">https://www.apple.com/macbook-air/specs/</a>
				</div>
			</div>
			<div class=""search-result-item"">
				<div class=""search-result-content"">
					<h3><a href=""#"">MacBook - Design - Apple</a></h3>
					<p>
						MacBook is the thinnest and lightest Mac notebook ever — each detail refined for maximum efficiency and portability.
					</p>
					<a href=""#"">https://www.apple.com/macbook/design/</a>
				</div>
			</div>
			<div class=""search-result-item"">
				<div class=""search-result-content"">
					<h3><a href=""#"">MacBook - Technical Specifications - Apple</a></h3>
					<p>
						View the technical specifications for MacBook. See what comes in the box and get a list of ");
            WriteLiteral(@"accessories.
					</p>
					<a href=""#"">https://www.apple.com/macbook/specs/</a>
				</div>
			</div>
			<div class=""search-result-item"">
				<div class=""search-result-content"">
					<h3><a href=""#"">MacBook - Wireless - Apple</a></h3>
					<p>
						MacBook is designed to make the most of the wireless world. Share files, browse the web, print, and more — without being tied down to a cable....
					</p>
					<a href=""#"">https://www.apple.com/macbook/wireless/</a>
				</div>
			</div>
			<div class=""search-result-item"">
				<div class=""search-result-content"">
					<h3><a href=""#"">MacBook - macOS - Apple</a></h3>
					<p>
						MacBook - macOS - Apple macOS. It’s why there’s nothing else like a Mac. macOS is the operating system that powers every Mac. It lets you do things you simply can’t with other computers. That’s...
					</p>
					<a href=""#"">https://www.apple.com/macbook/macos/</a>
				</div>
			</div>
		</div>
		<!-- END search-result-list -->
		
		<!-- BEGIN pagination -->
		<d");
            WriteLiteral(@"iv class=""text-center mt-4 mb-5"">
			<div class=""pagination justify-content-center"">
				<div class=""disabled page-item"">
					<a href=""#"" class=""page-link"">
						<span>«</span>
					</a>
				</div>
				<div class=""active page-item""><a href=""#"" class=""page-link"">1</a></div>
				<div class=""page-item""><a href=""#"" class=""page-link"">2</a></div>
				<div class=""page-item""><a href=""#"" class=""page-link"">3</a></div>
				<div class=""page-item""><a href=""#"" class=""page-link"">4</a></div>
				<div class=""page-item""><a href=""#"" class=""page-link"">5</a></div>
				<div class=""page-item"">
					<a href=""#"" class=""page-link"">
						<span>»</span>
					</a>
				</div>
			</div>
		</div>
		<!-- END pagination -->
	</div>
	<!-- END search-result -->
</div>
<!-- END search-result -->");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
