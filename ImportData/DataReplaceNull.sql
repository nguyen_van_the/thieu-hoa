UPDATE DonHangs
SET HoVaTen = ''
WHERE HoVaTen is NULL;

UPDATE DonHangs
SET SDT = 0
WHERE SDT is NULL;

UPDATE DonHangs
SET DiaChi = 'VN'
WHERE DiaChi is NULL;

UPDATE DonHangs
SET Size = 0
WHERE Size is NULL;

UPDATE DonHangs
SET TrangThai = 0
WHERE TrangThai is NULL;

UPDATE DonHangs
SET LuuYGiaoHang = ''
WHERE LuuYGiaoHang is NULL;